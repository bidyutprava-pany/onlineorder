<?php

include "config.php";

	function dbconnect()
	{
		global $username;
		global $password;
		global $host;
		global $dbname;

		$conn = mysqli_connect($host, $username, $password, $dbname);
// Check connection
		if (!$conn) {
   			 die("Connection failed: " . mysqli_connect_error());
		}
		return $conn;
	}

	
?>